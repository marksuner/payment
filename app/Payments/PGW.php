<?php
namespace App\Payments;

use GuzzleHttp\Client;
class PGW
{
  protected $url = '';
  protected $key = '';
  protected $token = '';
  protected $serviceId = '';
  protected $pemFilePath = '';

  protected $client = null;

  public static $STATUS_CODES = [
    '21' => 'Invalid Service',
    '22' => 'Some parameters posted to MyEG PGW are invalid or empty',
    '23' => 'The selected payment channel is not activated for the service',
    '25' => 'Invalid checksum',
    '35' => 'Invalid credit card',
    '36' => 'User cancel payment at payment channel selection page',

    '00' => 'Success',
    '26' => 'Payment declined by payment channel',
    '27' => 'Unknown error',
    '28' => 'Payment failed',
    '29' => 'Payment pending',
    '30' => 'Invalid amount',
  ];

  
  function __construct()
  {
    $this->url = config('payments.PGW.url');
    $this->key = config('payments.PGW.key');
    $this->token = config('payments.PGW.token');
    $this->serviceId = config('payments.PGW.service_id');
    $this->pemFilePath = config('payments.PGW.pem_file');


    $this->client = new Client([
      'headers' => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
        'Authorization' => 'Bearer '. $this->token,
      ]
    ]);
    
  }

  public function getToken() {
    return $this->token;
  }

  public function getKey() {
    return $this->key;
  }

  public function getServiceId() {
    return $this->serviceId;
  }

  /**
   * on generate stringify data for pgw payment gateway
   */
  protected function onGenerateStringifyData(array $data = []) {
    ksort($data);
    
    $stringify = '';

    foreach($data as $key => $value) {
      if (is_numeric($value)) {
        $stringify .= str_contains($value, '.')
          ? $value . '|'
          : number_format($value, '2', '.', '') . '|';
        
      } else {
        $stringify .= $value . '|';
      }
    }

    $stringify = substr($stringify, 0, -1);

    return $stringify;
  }

  /**
   * ideally the pem file should not be exposed in the server
   * but rather on a secured file storage / server such as s3 and alike with permission
   * to only allow certain people that can access this file.
   */
  protected function onGenerateCheckSum(string $data = '') {
    $private_key = openssl_pkey_get_private($this->pemFilePath);

    openssl_sign($data, $signature, $private_key, OPENSSL_ALGO_SHA256);

    return strtoupper(bin2hex($signature));
  }

  /**
   * should handle payment data, that includes checksum and such
   */
  protected function onHandlePaymentData(array $data = []) {
    $stringify = $this->onGenerateStringifyData($data);
    
    // add checksum to the data;
    $data['checksum'] = $this->onGenerateCheckSum($stringify);

    return $data;
  }

  /**
   * the actual payment transaction to PGW
   */
  public function pay(array $data = []) {
    $payment = $this->onHandlePaymentData($data);

    try {
      $response = $this->client->request('POST', $this->url . '/api/v1/payment/easy', $payment);
      
      $response = json_decode($response->getBody()->getContents(), true);

      $result = [
        'status_code' => $response['status_code'],
        'message' => self::$STATUS_CODES[$response['status_code']],
        'data' => $response['data'],
      ];
      
      return $result;

    } catch (RequestException $e) {
      throw new Exception($e->getRequest());
    }

  }
}