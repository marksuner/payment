<?php

return [
    'PGW' => [
        'url' => env('PGW_URL'),
        'key' => env('PGW_KEY'),
        'token' => env('PGW_TOKEN'),
        'service_id' => env('PGW_SERVICE_ID'),
        'pem_file' => env('PGW_PEM_FILE')
    ],
    'GCASH' => [
        'url' => env('GCASH_URL'),
        'version' => env('GCASH_VERSION'),
        'client_id' => env('GCASH_CLIENT_ID'),
        'client_secret' => env('GCASH_CLIENT_SECRET'),
    ]
];