<?php

namespace Tests\Unit\Payments;

use App\Payments\PGW;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PGWTest extends TestCase
{
    /**
     * test config of pgw service.
     *
     * @return void
     */
    public function testPgwConfig()
    {
        $pgw = new PGW();

        $this->assertSame(config('payments.PGW.key'), $pgw->getKey());
        $this->assertSame(config('payments.PGW.token'), $pgw->getToken());
        $this->assertSame(config('payments.PGW.service_id'), $pgw->getServiceId());
    }

    /**
     * test pgw check sum generation
     * 
     * @return void
     */
    public function testPgwStringifyData() {

        $class = new \ReflectionClass('\\App\\Payments\\PGW');
        $method = $class->getMethod('onGenerateStringifyData');
        $method->setAccessible(true);
        $onGenerateStringifyData = $method;

        $pgw = new PGW();
        
        $firstData = [
            'amount' => 100.0034,
            'currency' => 'PHP',
        ];
        
        $secondData = [
            'amount' => 1000.00,
            'currency' => 'PHP',
            'customer_email' => 'customer@gmail.com',
            'customer_name' => 'Ali Ahmad',
            'customer_id' => 'cust_112',
            'order_no' => 'ABC123',
            'product_descriptionn' => 'Monitor',
            'service_id' => 'SPH0000001',
            'transaction_time' => '2018-03-04 14:04:02'
        ];

        $firstResult = $onGenerateStringifyData->invokeArgs($pgw, [$firstData]);
        $secondResult = $onGenerateStringifyData->invokeArgs($pgw, [$secondData]);

        $this->assertSame($firstResult, '100.0034|PHP');
        $this->assertSame($secondResult, '1000.00|PHP|customer@gmail.com|cust_112|Ali Ahmad|ABC123|Monitor|SPH0000001|2018-03-04 14:04:02');
    }
}
